#!/usr/local/bin/python3.5

import re
from decimal import localcontext, Decimal

ops = {
    '^': lambda x, y: x ** y,
    '*': lambda x, y: x *  y,
    '/': lambda x, y: x /  y,
    '+': lambda x, y: x +  y,
    '-': lambda x, y: x -  y
}

class MathError(Exception):
    pass

class NumberError(MathError, Exception):
    pass

class OperatorError(MathError, Exception):
    pass

class ParsingError(MathError, Exception): # SyntaxError is taken
    pass

class PyCalc:
    def __init__(self, precision, debug = False):
        self.precision = precision
        self.debug = debug

    def __repr__(self):
        return 'pycalc.PyCalc({})'.format(
            self.precision if not self.debug else self.precision + ', True'
        ) # http://stackoverflow.com/a/2626364/6516765
          # "If you have enough information so `eval(repr(c))==c`,
          # that means you know everything there is to know about `c`."

    def sep(self, string):
        matches = re.finditer(r'(?P<num>-?[0123456789\.]+)|(?P<op>[\^*\/+-])|(?P<paren>[\(\)])', string)
        data = []

        for matchNum, match in enumerate(matches):
            for groupName in ['num', 'op', 'paren']:
                group = match.group(groupName)

                if group:
                    data.append((groupName, str(group)))

        if self.debug:
            print(data)

        return data

    def evaluate(self, string):
        with localcontext() as context:
            context.prec = self.precision

            for i in string:
                if i.strip() != '' and not i in '0123456789.^*/+-()':
                    raise ParsingError('syntax error near unexpected token `' + i + '\'')

            if not string.count('(') == string.count(')'):
                raise ParsingError('unbalanced parenthesis')
            else:
                if string.count('(') > 0:
                    without = re.findall(r'^(.*?)\(.*\)(.*?)$', string)[0]
                    only = re.findall(r'^.*?\((.*)\).*?$', string)[0]

                    if self.debug:
                        print('found parenthesis')
                        print('without: ' + str(without))
                        print('only   : ' + str(only))

                    string = str(without[0]) + str(self.evaluate(only)) + str(without[1])

            data = self.sep(string)

            last = None
            op = None

            for i in data:
                if i[0] == 'op':
                    if not op:
                        op = i
                    else:
                        raise OperatorError('too many operators')
                elif i[0] == 'num':
                    num = None

                    try:
                        num = context.create_decimal(i[1])
                    except:
                        raise NumberError('invalid number {}'.format(i[1]))

                    if not op:
                        if not last:
                            last = num
                        else:
                            raise NumberError('too many numbers')
                    else:
                        if self.debug:
                            print(op[1] + 'ing ' + str(last) + ' by ' + str(num))

                        last = ops[op[1]](last, num)
                        op = None

            if self.debug:
                print('returning ' + str(last) + ' for ' + string)

            return str(last)

if __name__ == '__main__':
    print('Welcome to the PyCalc interpreter!')

    calc = PyCalc(20)

    while True:
        try:
            i = input('\n> ')
        except (KeyboardInterrupt, EOFError):
            print()
            break

        if i.strip() == '':
            break
        else:
            try:
                print(calc.evaluate(i))
            except MathError as e:
                print('Error: ' + str(e))
