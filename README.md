## PyCalc

A simple Python library for evaluating written expressions.

## How to use

The script can be imported as a module or run standalone.

#### Running as a module

You can import the script like any other normal Python module, and create an instance of the `PyCalc` class like so:

```py
>>> import pycalc
>>> calc = pycalc.PyCalc(20)
>>> calc
pycalc.PyCalc(20)
>>> 
```

Above, I use a precision of 20 decimal places. This can be raised for insanely precise calculations, but I wouldn't go further than 20 for casual use.

This class has 2 methods: `sep` and `evaluate`. `evaluate` is what we'll be using the most. `sep` is used internally by `evaluate` to seperate the string into usable portions readable by the interpreter.

```py
>>> calc.evaluate('2 * (3 / 2)')
'3.0'
>>> 
```

```py
>>> calc.sep('2 * (3 / 2)')
[('num', '2'), ('op', '*'), ('paren', '('), ('num', '3'), ('op', '/'), ('num', '2'), ('paren', ')')]
>>> 
```

`evaluate` reads the expression from left to right, ignoring [PEMDAS](https://en.wikipedia.org/wiki/Order_of_operations#Mnemonics). However, it most certainly does evaluate expressions in parenthesis first.

```py
>>> calc.evaluate('2 + (3 * 2)')
'8'
>>> 
```

PyCalc supports all 5 basic operations: pow (exponentiation), multiplication, devision, addition and subtraction.

```py
>>> calc.evaluate('(2^4) + (2*4) + (2/4) + (2+4) + (2- 4)')
'24.5'
>>> 
```

> PyCalc returns results as strings, due to the nature of the special numbering system (read: magic) it uses under the hood. These can easily be converted to `int`s or `float`s though.

> Using a subtraction operator directly next to a number is illadvised, due to the possibility of it being recognized as a negative number.

## code pls kthx

```py
from pycalc import PyCalc, MathError

calc = PyCalc(20)

while True:
    exp = input('\n> ')

    if exp.strip() != '':
        try:
            print(calc.evaluate(exp))
        except MathError as ex:
            print('Error: ' + str(ex))
    else:
        break
```